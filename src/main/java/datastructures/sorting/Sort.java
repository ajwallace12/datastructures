package datastructures.sorting;

public interface Sort {
    void sortIntegerArray(int[] array);
}
