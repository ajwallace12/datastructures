package datastructures.list;

public interface List {
    void add(Object value);
    Object getLast();
    Object getFirst();
}
