package datastructures.node;

public class SingleListNode {
    public SingleListNode next;
    public Object data;
    public SingleListNode(Object data) {
        this.data = data;
    }
}
