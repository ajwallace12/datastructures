package datastructures.map;

import datastructures.node.Bucket;

public class HashMap implements Map {

    private final Bucket[] buckets;
    private final int capacity;

    public HashMap(int capacity) {
        this.capacity = capacity;
        buckets = new Bucket[capacity];
    }

    @Override
    public void put(Object key, Object value) {
        int hash = hash(key);
        Bucket activeBucket = buckets[hash];
        Bucket insert = new Bucket(key, value);
        if(activeBucket == null)
            buckets[hash] = insert;
        else
            while(activeBucket.next != null) {
                if(activeBucket.key.equals(key)) {
                    activeBucket.value = value;
                    return;
                }
                activeBucket = activeBucket.next;
            }
            activeBucket.next = insert;
    }

    private int hash(Object key) {
        return key.hashCode() % capacity;
    }

    @Override
    public Object get(Object key) {
        int hash = hash(key);
        Bucket bucket = buckets[hash];
        if(bucket == null)
            return null;
        while(bucket.next != null) {
            if(bucket.key.equals(key))
                return bucket.value;
            bucket = bucket.next;
        }
        return null;
    }
}
