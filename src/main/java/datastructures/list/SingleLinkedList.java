package datastructures.list;

import datastructures.node.SingleListNode;

public class SingleLinkedList implements List {
    private SingleListNode head;

    @Override
    public void add(Object value) {
        SingleListNode endNode = new SingleListNode(value);
        if(head == null)
            head = endNode;
        else {
            SingleListNode lastNode = getLastNode();
            lastNode.next = endNode;
        }
    }

    private SingleListNode getLastNode() {
        if(head == null)
            return null;

        SingleListNode currentNode = head;
        while(currentNode.next != null)
            currentNode = currentNode.next;
        return currentNode;
    }

    @Override
    public Object getLast() {
        SingleListNode lastNode = getLastNode();
        if(lastNode == null)
            return null;

        return getLastNode().data;
    }

    @Override
    public Object getFirst() {
        if(head == null)
            return null;
        return head.data;
    }
}
