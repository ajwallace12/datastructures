package datastructures.examples;

import datastructures.list.List;
import datastructures.list.SingleLinkedList;
import datastructures.map.HashMap;
import datastructures.map.Map;

public class PracticeRunner {
    public static void main(String[] args) {
        List singleLinkedList = new SingleLinkedList();
        singleLinkedList.add(1);
        singleLinkedList.add(2);
        singleLinkedList.add(3);
        System.out.println(singleLinkedList.getFirst());
        System.out.println(singleLinkedList.getLast());
        Map map = new HashMap(10);
    }
}
