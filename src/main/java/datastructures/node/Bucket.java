package datastructures.node;

public class Bucket {
    public Object key;
    public Object value;
    public Bucket next;

    public Bucket(Object key, Object value) {
        this.key = key;
        this.value = value;
    }
}
