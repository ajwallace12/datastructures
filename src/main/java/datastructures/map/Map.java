package datastructures.map;

public interface Map {
    void put(Object key, Object value);
    Object get(Object key);
}
