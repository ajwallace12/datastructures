package datastructures.sorting;

public class QuickSort implements Sort {

    private int[] activeArray;

    @Override
    public void sortIntegerArray(int[] array) {
        this.activeArray = array;
        recursiveSort(0, array.length - 1);
    }

    private void recursiveSort(int low, int high) {
        // create a pivot value then we do one search over and swap elements left to right
        // then work on the sub parts
        int i = low;
        int j = high;
        int pivot = activeArray[low + (high - low) / 2];

        while(i <= j) {
            while(activeArray[i] < pivot)
                i++; // keep going to find a bigger value

            while(activeArray[j] > pivot)
                j--; // keep going to find a smaller value

            if(i <= j) {
                int temp = activeArray[i];
                activeArray[i] = activeArray[j];
                activeArray[j] = temp;
                i++;
                j--;
            }
        }

        if(low < j)
            recursiveSort(low, j);
        if(i < high)
            recursiveSort(i, high);

    }
}
